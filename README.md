# Docker Images for PACXX

This repository provides docker images for PACXX.  Three kinds of images are
provided:
- `base-native`, `base-cuda`: Contain all the necessary software to compile
  PACXX, based on the images `ubuntu:16.04` and
  `nvidia/cuda:9.0-devel-ubuntu16.04`, respectively
- `pacxx-native`, `pacxx-cuda`: based on the respective base images, contain
  the compiled pacxx installed in `/opt/pacxx`
- `pacxx-perftest-2.6`: based on `duneci/base:16.04`, contains the compiled
  native pacxx in `/opt/pacxx`, as well as a 2.6 [dune-common][] and
  [dune-perftest][] compiled from the `pacxx-measure` branch.  Meant for
  modules based on the `pacxx-measure` branch of [dune-perftest][].

The native images are based on the stock `ubuntu:16.04` docker images, and
compile/provide the native PACXX backend only.  These should work on any
installation out of the box.

The cuda images are based on `nvidia/cuda:9.0-devel-ubuntu16.04` images.  They
can unsually compile pacxx-based programs using dockers default runtime, but
for executing those programs they need the special
[nvidia runtime][nvidia-docker].  In addition to the CUDA PACXX backend they also
include the native PACXX backend, and do not need actual access to an NVIDIA
GPU when using the native backend, though the nvidia runtime is still required.
(Compiled programs will still link against the CUDA runtime libraries, and
those are made available at runtime by the nvidia docker runtime.)

[dune-common]: https://gitlab.dune-project.org/core/dune-common
[dune-perftest]: https://gitlab.dune-project.org/infrastructure/dune-perftest
[nvidia-docker]: https://github.com/nvidia/nvidia-docker/wiki/Installation-(version-2.0)

## Tags

All images are tags of the repository
`registry.dune-project.org/joe/pacxx-docker`.  Their tag name begins with the
_kind_ (as above) and has a dot and the branch name appended.  So to get the
`pacxx-native` image or branch master, do
```sh
docker pull registry.dune-project.org/joe/pacxx-docker:pacxx-native.master
```
In addition, there are temporary tags that have an additional dot and a number
appended, like `pacxx-native.master.332`.  These tags are used internally by
the CI system to build and test the images (the number is the pipeline id).

The reason for keeping everything in the same repository (rather than having
separate repositories for different kinds of images or for different branches)
is that the docker registry used by gitlab does not automatically reuse layers
from a different repository.  Clients pushing images must upload layers
completely, even if they are already present in a different repository on the
same registry.  This can only be avoided if the layer is already present in
the same repository, or the client knows a repository where the layer is
present, is authorized to access that repository, and explicitly mounts the
layer from the source repository into the target repository.  (This
requirement is for security reasons to avoid side-channels leaking information
about the presense of layers in the repositories the client does not have
access to.)

## Cleanup

For the CI system, images are organized into environments so they can be
automatically clean up.

Each branch has it's own environment with the same name as the branch, that
collect the non-temporary images of that branch, e.g. `base-native.<branch>`,
`base-cuda.<branch>`, `pacxx-native.<branch>`, `pacxx-cuda.<branch>`, and
`pacxx-perftest-2.6.<branch>`.  These environments are closed and the images
are trashed when a branch is merged or deleted.

While a pipeline is running, it puts temporary tags (those that have a
pipeline id in their name) in an environment formed from the branch and stage
name: `tmp/<branch>/<stage>`, where `<stage>` is either `base` (for the
`base-*` tags) or `mkimage` (for the `pacxx-*` tags).  Before doing so, it
will however trash any old tags found in that environment -- these temporary
environments are only relevant for the cleanup of branches that managed to
build temporary tags but never got past the tests, and thus never created a
branch environment, so that the branch environment cleanup won't run.

Trashing of tags is done by uploading a dummy image to the same tag.  Such
images will appear to have a size of 0 bytes and as having been built on
1970-01-01.  These images will be removed nightly by a schedule in a private
repository.  There are two reasons for this construction:
- The registry only allows deletion of images, not tags.  Deleting one image
  (through a tag name referring to that image) will get rid of all other tags
  referring to the same image.  Uploading dummy images under the to-be-deleted
  tag name is the workaround for that.
- GitLab allows deletion of images from the registry only with an API key of a
  user, which basically allows full control over that users account.  The
  normal CI token only allows pulling and pushing or images.
